package sudoku;

public interface SudokuSolver {
		/**
		 * Attempts to recursively solve the Sudoku by iterating through each square and checking if a solution is valid or not.
		 * Returns true if a solution exists, else it returns false;
		 */
	boolean solve();
	
		/**
		 * Puts digit in the square row, col.
		 * 
		 * @param row   The row
		 * @param col   The column
		 * @param digit The digit to insert in box row, col
		 * @throws IllegalArgumentException if row, col or digit is outside the range
		 *                                  [0..9]
		 */
	void add(int row, int col, int digit);

		/**
		 * Removes digit from a square row, col. 
		 */
	void remove(int row, int col);

		/**
		 * Returns the digit in the square row, col;
		 */
	int get(int row, int col);

		/**
		 * Checks that all filled in digits follows the the sudoku rules.
		 */
	boolean isValid();

		/**
		 * Clears all squares.
		 */
	void clear();

		/**
		 * Fills the grid with the digits in m. The digit 0 represents an empty box.
		 * 
		 * @param m the matrix with the digits to insert
		 * @throws IllegalArgumentException if m has the wrong dimension or contains
		 *                                  values outside the range [0..9]
		 */
	void setMatrix(int[][] m);

		/**
		 * Returns the current Sudoku matrix.
		 */
	int[][] getMatrix();
	}