package sudoku;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class SudokuGUI extends JFrame implements ActionListener {
	private JTextField[][] squares;
	private JButton solveButton;
	private JButton clearButton;
	private SudokuSolver solver = new SudokuReader();
	
	
	public SudokuGUI(SudokuSolver solver) {
		this.solver = solver;
		squares = new JTextField[9][9];
		JPanel gridPanel = new JPanel(new GridLayout(9,9));
        gridPanel.setBackground(Color.BLACK);
        squares = new JTextField[9][9];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                JTextField square = new JTextField(1);
                square.setHorizontalAlignment(JTextField.CENTER);
                square.setBackground(Color.WHITE);
                squares[i][j] = square;
                gridPanel.add(square);
                
                square.setFont(new Font("Arial", Font.BOLD,50));
                square.setBorder(BorderFactory.createLineBorder(Color.BLACK, 4));
                
                if (((i < 3 || i > 5) && (j < 3 || j > 5)) || ((i > 2 && i < 6) && (j > 2 && j < 6))) {
                	square.setBackground(Color.GRAY);
                }
            }
        }

        solveButton = new JButton("Solve");
        solveButton.addActionListener(this);
        
        clearButton = new JButton("Clear");
        clearButton.addActionListener(this);

        JPanel buttonPanel = new JPanel(new FlowLayout());
        buttonPanel.add(solveButton);
        buttonPanel.add(clearButton);
        add(buttonPanel, BorderLayout.SOUTH);
        add(gridPanel, BorderLayout.CENTER);
        
        
        setTitle("Sudoku");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setSize(800,800);
        setVisible(true);
    }
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == solveButton) { // SolveButton
			int[][] currentState = solver.getMatrix();//new int[9][9];
	
			for (int i = 0; i < 9; i++) { // Check for false input
                for (int j = 0; j < 9; j++) {
                    String text = squares[i][j].getText();
                    
                    if (text.isEmpty()) {
                        currentState[i][j] = 0;
                    } else {
                    	 try {
                             int num = Integer.parseInt(text);
                             if (num < 1 || num > 9) {
                                 throw new NumberFormatException();
                             }
                             currentState[i][j] = num;
                         } catch (NumberFormatException ex) {
                             JOptionPane.showMessageDialog(this, "Please enter a number between 1 and 9!");
                             return;
                         }
                    	 
//                    		int num = Integer.parseInt(text);
//                    		if ((num < 1 || num > 9)) {
//                    			JOptionPane.showMessageDialog(this, "Please enter a number between 1 and 9!");
//                    			return;
//                    	}
//                        currentState[i][j] = num;
                    }
                }
			}
            solver.setMatrix(currentState); 
            
            if (solver.solve()) { // Solve
            		int[][] solution = solver.getMatrix();
            		for (int i = 0; i < 9; i++) {
            			for (int j = 0; j < 9; j++) {
            				squares[i][j].setText(Integer.toString(solution[i][j]));
            		}
            	}
           	} else { // Unsolvable
           		JOptionPane.showMessageDialog(this, "Sudoku cannot be Solved!");
                solver.clear();
            	}
            
		} else if (e.getSource()== clearButton) { // ClearButton
			solver.clear();
			for (int i = 0; i<9; i++) {
				for (int j = 0; j < 9; j++) {
					squares[i][j].setText("");
				}
			}
		}
	}

	public static void main(String[] args) {
		SudokuSolver solver = new SudokuReader();
		new SudokuGUI(solver);
		System.out.println("run");
	}
}
