package sudoku;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SudokuReaderTest {
	private SudokuSolver solver = new SudokuReader();
	
	@Test
	void testSolveEmptySudoku() {
		assertTrue(solver.solve());
	}
	
	@Test
	void testSolveFig1Sudoku() {
		int[][] matrix = {
				{0,0,8, 0,0,9, 0,6,2},
				{0,0,0, 0,0,0, 0,0,5},
				{1,0,2, 5,0,0, 0,0,0},
				
				{0,0,0, 2,1,0, 0,9,0},
				{0,5,0, 0,0,0, 6,0,0},
				{6,0,0, 0,0,0, 0,2,8},
				
				{4,1,0, 6,0,8, 0,0,0},
				{8,6,0, 0,3,0, 1,0,0},
				{0,0,0, 0,0,0, 4,0,0},
		};
		solver.setMatrix(matrix);
		assertTrue(solver.solve());
		int[][] solved = solver.getMatrix();
		int[][] expected = {
				{5,4,8, 1,7,9, 3,6,2},
				{3,7,6, 8,2,4, 9,1,5},
				{1,9,2, 5,6,3, 8,7,4},
				
				{7,8,4, 2,1,6, 5,9,3},
				{2,5,9, 3,8,7, 6,4,1},
				{6,3,1, 9,4,5, 7,2,8},
				
				{4,1,5, 6,9,8, 2,3,7},
				{8,6,7, 4,3,2, 1,5,9},
				{9,2,3, 7,5,1, 4,8,6},

		};
		assertArrayEquals(expected, solved);
	}
	
	@Test
	void testSolveUnsolvableSudoku() {
		int[][] matrix = {
				{1,1,1, 1,1,1, 1,6,2},
				{0,0,0, 0,0,0, 0,0,5},
				{1,0,2, 5,0,0, 0,0,0},
				
				{0,0,0, 2,1,0, 0,9,0},
				{0,5,0, 0,0,0, 6,0,0},
				{6,0,0, 0,0,0, 0,2,8},
				
				{4,1,0, 6,0,8, 0,0,0},
				{8,6,0, 0,3,0, 1,0,0},
				{0,0,0, 0,0,0, 4,0,0},
		};
		solver.setMatrix(matrix);
		assertFalse(solver.solve());
	}
	
	@Test
	void testAdd() {
		solver.add(0, 0, 1);
		assertEquals(1, solver.get(0,0));
	}
	@Test
	void testRemove() {
		solver.add(0, 0, 1);
		solver.remove(0, 0);
		assertEquals(0, solver.get(0,0));
	}
	@Test
	void testIsValid() {
		assertTrue(solver.isValid(), "Test if Valid");
		
		solver.add(0, 0, 1);
		solver.add(1, 0, 1);
		assertFalse(solver.isValid(), "Test Column");
		solver.remove(1, 0);
		
		solver.add(0, 0, 1);
		solver.add(0, 1, 1);
		assertFalse(solver.isValid(), "Test Row");
		solver.remove(0, 0);
		
		solver.add(0, 0, 1);
		solver.add(1, 1, 1);
		assertFalse(solver.isValid(), "Test 3x3 Square");
	}
	
	@Test
	void testClear() {
		solver.add(0, 0, 1);
		solver.clear();
		assertEquals(0, solver.get(0,0));
	}

}
