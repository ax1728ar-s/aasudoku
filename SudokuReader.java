package sudoku;

class SudokuReader implements SudokuSolver {
	
	private int[][] matrix;
	private int backtrackCount;

    
	public SudokuReader() {
		this.matrix = new int[9][9];
		this.backtrackCount = 0;
	}
	
	@Override
	public boolean solve() {
		backtrackCount = 0;
		return solve(0,0); //BaseCase
	}
	
	private boolean solve(int row, int col) {
	    System.out.println("solve(" + row + ", " + col + ")");
		if (col == 9) { // Iterate through the matrix
			row++;
			col = 0;
			if (row == 9) {
				return true; // Sudoku solved.
			}
		}
		if (backtrackCount > 1000000) {
			//backtrackCount = 0;
			return false;
		}
		
		if (matrix[row][col] != 0) { //If square filled
			return solve(row,col+1);
		}
			 //solve next square
		for (int digit = 1; digit <= 9; ++digit) { //test digits 1 to 9
			if(isValid(row, col, digit)) { //if isValid, add digit 
				add(row,col,digit);
				if (solve(row, col+1)) { //if next square not solved, remove this square
					return true;
				}
				System.out.println("remove");
				remove(row,col);
				backtrackCount++;
			}			
		}
		System.out.println("false");
		return false; // Unsolvable
	}	


	@Override
	public void add(int row, int col, int digit) {
		if (digit < 0 || digit > 9) {
			throw new IllegalArgumentException("Digit not between 1 and 9!");
			}
//		if (!isValid(row, col, digit)) {
//			throw new IllegalArgumentException("Digit is not valid for this square!");
//		}
		matrix[row][col] = digit;
	}

	@Override
	public void remove(int row, int col) {
		matrix[row][col] = 0;
	}

	@Override
	public int get(int row, int col) {
		return matrix[row][col];

	}

	@Override
	public boolean isValid() {
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				int digit = get(i, j);
				if (digit != 0 && !isValid(i,j,digit)) {
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean isValid(int row, int col, int digit) {
		for (int i = 0; i < 9; i++) { // Check Rows and Columns
			if (matrix[row][i] == digit || matrix[i][col] == digit) {
				return false;
			}
		}
		int boxRow = (row/3)*3; // Check 3by3 boxes
		int boxCol = (col/3)*3;
		for (int i = boxRow; i < boxRow + 3; i++) {
			for (int j = boxCol; j < boxCol + 3; j++) {
				if (matrix[i][j] == digit) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public void clear() {
		this.matrix = new int[9][9];
		}	

	@Override
	public void setMatrix(int[][] m) {
		if (m.length != 9 || m[0].length != 9) {
			throw new IllegalArgumentException("Matrix must be a 9 by 9!");
		}
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				int digit = m[i][j];
				if (digit != 0) {
					add(i, j, m[i][j]);
				}
		    }
		}
	}	

	@Override
	public int[][] getMatrix() {
		int[][] matrix = new int[9][9];
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				matrix[i][j] = get(i, j);
			}
		}
		return matrix;
	}

}
